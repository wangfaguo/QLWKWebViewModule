# QLWKWebViewModule

[![CI Status](http://img.shields.io/travis/wangfaguo/QLWKWebViewModule.svg?style=flat)](https://travis-ci.org/wangfaguo/QLWKWebViewModule)
[![Version](https://img.shields.io/cocoapods/v/QLWKWebViewModule.svg?style=flat)](http://cocoapods.org/pods/QLWKWebViewModule)
[![License](https://img.shields.io/cocoapods/l/QLWKWebViewModule.svg?style=flat)](http://cocoapods.org/pods/QLWKWebViewModule)
[![Platform](https://img.shields.io/cocoapods/p/QLWKWebViewModule.svg?style=flat)](http://cocoapods.org/pods/QLWKWebViewModule)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

QLWKWebViewModule is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "QLWKWebViewModule"
```

## Author

wangfaguo, 452290424@qq.com

## License

QLWKWebViewModule is available under the MIT license. See the LICENSE file for more info.
