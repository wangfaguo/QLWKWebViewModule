#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "QLJsNativeBaseResponder.h"
#import "QLUIWebViewManager.h"
#import "QLWebViewHeader.h"
#import "QLWKWebViewManager.h"

FOUNDATION_EXPORT double QLWKWebViewModuleVersionNumber;
FOUNDATION_EXPORT const unsigned char QLWKWebViewModuleVersionString[];

