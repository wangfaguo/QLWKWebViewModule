//
//  QLJSNativeResponder.h
//  QLWKWebViewModule
//
//  Created by wangfaguo on 17/1/14.
//  Copyright © 2017年 wangfaguo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QLWKWebViewModule/QLWebViewHeader.h>

@interface QLJSNativeResponder : QLJsNativeBaseResponder

-(void)showTitle:(NSDictionary*)params;

@end
