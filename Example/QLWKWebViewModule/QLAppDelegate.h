//
//  QLAppDelegate.h
//  QLWKWebViewModule
//
//  Created by wangfaguo on 01/14/2017.
//  Copyright (c) 2017 wangfaguo. All rights reserved.
//

@import UIKit;

@interface QLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
