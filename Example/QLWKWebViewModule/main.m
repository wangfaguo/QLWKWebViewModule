//
//  main.m
//  QLWKWebViewModule
//
//  Created by wangfaguo on 01/14/2017.
//  Copyright (c) 2017 wangfaguo. All rights reserved.
//

@import UIKit;
#import "QLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([QLAppDelegate class]));
    }
}
