//
//  QLViewController.m
//  QLWKWebViewModule
//
//  Created by wangfaguo on 01/14/2017.
//  Copyright (c) 2017 wangfaguo. All rights reserved.
//

#import "QLViewController.h"
#import <WebKit/WebKit.h>
#import "QLJSNativeResponder.h"
#import <QLWKWebViewModule/QLWebViewHeader.h>

#define isIOS8 ([[[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."] objectAtIndex:0] intValue] >= 8)


@interface QLViewController ()
@property (nonatomic,strong) QLWKWebViewManager *wkWebViewManager;
@property (nonatomic,strong) QLUIWebViewManager *uiWebViewManager;
@property (nonatomic,strong) WKWebView *wkWebView;
@property (nonatomic,strong) UIWebView *uiWebView;
@end

@implementation QLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    if (isIOS8) {
        self.wkWebViewManager = [[QLWKWebViewManager alloc]initWithFrame:self.view.frame controller:self];
        [self.wkWebViewManager addJS:[QLJSNativeResponder class] method:@"showTitle"];
        [self.wkWebViewManager addJS:[QLJSNativeResponder class] method:@"getCode"];
        self.wkWebView = self.wkWebViewManager.webView;
        [self.view addSubview:self.wkWebView];
        NSString *resourcePath = [ [NSBundle mainBundle] pathForResource:@"WebView" ofType:@""];
        NSString *filePath =[resourcePath stringByAppendingPathComponent:@"index.html"];
        NSString*htmlstring=[[NSString alloc] initWithContentsOfFile:filePath  encoding:NSUTF8StringEncoding error:nil];
        [self.wkWebView loadHTMLString:htmlstring baseURL:[NSURL fileURLWithPath:[ [NSBundle mainBundle] pathForResource:@"WebView" ofType:@""]]];
//       NSString *url =  @"http://192.168.1.103:82/loancalc/index.html";
//        [self.wkWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    }else{
        self.uiWebViewManager = [[QLUIWebViewManager alloc]initWithFrame:self.view.frame controller:self];
        [self.uiWebViewManager addJS:[QLJSNativeResponder class] method:@"showTitle"];
        self.uiWebView = self.uiWebViewManager.webView;
        [self.view addSubview:self.uiWebView];
        NSString *resourcePath = [ [NSBundle mainBundle] pathForResource:@"WebView" ofType:@""];
        NSString *filePath =[resourcePath stringByAppendingPathComponent:@"index.html"];
        NSString*htmlstring=[[NSString alloc] initWithContentsOfFile:filePath  encoding:NSUTF8StringEncoding error:nil];
        [self.wkWebView loadHTMLString:htmlstring baseURL:[NSURL fileURLWithPath:[ [NSBundle mainBundle] pathForResource:@"WebView" ofType:@""]]];
    }

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
