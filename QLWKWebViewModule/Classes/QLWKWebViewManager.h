//
//  QLWKWebViewManager.h
//  Pods
//
//  Created by wangfaguo on 17/1/14.
//
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>
#import "QLJsNativeBaseResponder.h"

@protocol QLWKWebViewDelegate <NSObject>


@end

@interface QLWKWebViewManager : NSObject<WKUIDelegate,WKNavigationDelegate,WKScriptMessageHandler>

@property (nonatomic,strong,readonly) WKWebView *webView;

-(instancetype)initWithFrame:(CGRect)frame controller:(UIViewController*)controller;
-(void)addJS:(Class)responder method:(NSString*)method;
@end
