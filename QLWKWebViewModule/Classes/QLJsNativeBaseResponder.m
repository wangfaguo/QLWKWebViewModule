//
//  QLJsNativeBaseResponder.m
//  Pods
//
//  Created by wangfaguo on 17/1/14.
//
//

#import "QLJsNativeBaseResponder.h"
@interface QLJsNativeBaseResponder()
@property (nonatomic,strong,readwrite) id webView;
@property (nonatomic,strong,readwrite) UIViewController *controller;
@end

@implementation QLJsNativeBaseResponder
-(instancetype)initWithWebView:(id)webView controller:(UIViewController *)controller{
    self = [super init];
    if (self) {
        self.webView = webView;
        self.controller = controller;
    }
    return self;
}
//js日志打印，
-(void)log:(id)data{
    NSLog(@"console log = %@",data);
}
@end
