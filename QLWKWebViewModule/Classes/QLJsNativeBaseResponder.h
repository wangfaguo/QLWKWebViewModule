//
//  QLJsNativeBaseResponder.h
//  Pods
//
//  Created by wangfaguo on 17/1/14.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@protocol QLWKWebViewProtocol <NSObject>
@end

@interface QLJsNativeBaseResponder : NSObject<QLWKWebViewProtocol>
@property (nonatomic,strong,readonly) id webView;
@property (nonatomic,strong,readonly) UIViewController *controller;

-(instancetype)initWithWebView:(id)webView controller:(UIViewController*)controller;
-(void)log:(id)data;
@end
