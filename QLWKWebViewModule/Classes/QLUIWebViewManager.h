//
//  QLUIWebViewManager.h
//  Pods
//
//  Created by wangfaguo on 17/1/16.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <JavaScriptCore/JavaScriptCore.h>

@interface QLUIWebViewManager : NSObject<UIWebViewDelegate>

@property (nonatomic,strong,readonly) UIWebView *webView;
@property (nonatomic,strong,readonly) JSContext *jsContext;

-(instancetype)initWithFrame:(CGRect)frame controller:(UIViewController*)controller;
-(void)addJS:(Class)responder method:(NSString*)method;
@end
