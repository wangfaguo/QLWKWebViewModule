//
//  QLWKWebViewManager.m
//  Pods
//
//  Created by wangfaguo on 17/1/14.
//
//

#import "QLWKWebViewManager.h"

@interface QLWKWebViewManager ()
@property (nonatomic,strong,readwrite) WKWebView *webView;
@property (nonatomic,weak) UIViewController *controller;
@property (nonatomic,strong) WKUserContentController *userContent;
@property (nonatomic,strong) NSMutableDictionary<NSString *,Class> *responders;
@end

@implementation QLWKWebViewManager
-(instancetype)initWithFrame:(CGRect)frame controller:(UIViewController *)controller{
    self = [super init];
    if (self) {
        self.responders = [[NSMutableDictionary alloc]init];
        self.controller = controller;
        self.userContent = [[WKUserContentController alloc]init];
        NSString *jsScript = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);";
        WKUserScript *wkUScript = [[WKUserScript alloc]initWithSource:jsScript injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
        [self.userContent addUserScript:wkUScript];
        
        //创建native对象，添加到js context中
        NSString *jsString = @"var native = {}; window.native = native;";
        WKUserScript *nativeScript = [[WKUserScript alloc]initWithSource:jsString injectionTime:WKUserScriptInjectionTimeAtDocumentStart forMainFrameOnly:YES];
        [self.userContent addUserScript:nativeScript];
        //添加log方法
        [self addJS:[QLJsNativeBaseResponder class] method:@"log"];
        //
        WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc]init];
        config.userContentController = self.userContent;
        self.webView = [[WKWebView alloc]initWithFrame:frame configuration:config];
        self.webView.navigationDelegate = self;
        self.webView.UIDelegate = self;
    }
    return self;
}

-(void)addJS:(Class)responder method:(NSString*)method{
    NSString *js = [NSString stringWithFormat:@"native.%@ = function(params){ window.webkit.messageHandlers.%@.postMessage(params);}",method,method];
    WKUserScript *jsScript = [[WKUserScript alloc]initWithSource:js injectionTime:WKUserScriptInjectionTimeAtDocumentStart forMainFrameOnly:YES];
    [self.userContent addUserScript:jsScript];
    [self.userContent addScriptMessageHandler:self name:method];
    self.responders[method] = responder;
}

-(void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message{
    NSString *method = message.name;
    Class moduleClass = self.responders[method];
    if ([moduleClass conformsToProtocol:@protocol(QLWKWebViewProtocol)]) {
        id obj = [[moduleClass alloc] initWithWebView:self.webView controller:self.controller];
        if ([obj isKindOfClass:[QLJsNativeBaseResponder class]]) {
            NSString *methodSelector = [NSString stringWithFormat:@"%@:",method];
            if ([obj respondsToSelector: NSSelectorFromString(methodSelector)]) {
                UIViewController *controller = self.controller;
                id params = message.body;
                if (params) {
                     [obj performSelector: NSSelectorFromString(methodSelector) withObject:params];
                }else{
                    [obj performSelector: NSSelectorFromString(methodSelector) withObject:nil];
                }
                
            }
        }else{
            NSLog(@"没有找到方法");
        }
        
    }else{
        NSLog(@"没有找到类");
    }
    
    
}

-(void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler{
    completionHandler();
}
@end
