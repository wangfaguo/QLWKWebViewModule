//
//  QLUIWebViewManager.m
//  Pods
//
//  Created by wangfaguo on 17/1/16.
//
//

#import "QLUIWebViewManager.h"
#import "QLJsNativeBaseResponder.h"

@interface QLUIWebViewManager ()
@property (nonatomic,strong,readwrite) UIWebView *webView;
@property (nonatomic,strong,readwrite) JSContext *jsContext;
@property (nonatomic,weak) UIViewController *controller;
//@property (nonatomic,strong) WKUserContentController *userContent;
@property (nonatomic,strong) NSMutableDictionary<NSString *,Class> *responders;
@end

@implementation QLUIWebViewManager
-(instancetype)initWithFrame:(CGRect)frame controller:(UIViewController *)controller{
    self = [super init];
    if (self) {
        self.responders = [[NSMutableDictionary alloc]init];
        self.controller = controller;
        self.webView = [[UIWebView alloc]initWithFrame:frame];
        self.webView.delegate = self;
    }
    return self;
}

-(void)addJS:(Class)responder method:(NSString*)method{
    self.responders[method] = responder;
}
#pragma mark - 
-(void)webViewDidStartLoad:(UIWebView *)webView{
    
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    self.jsContext = [webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    __weak QLUIWebViewManager *weakSelf = self;
    [self.responders enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull method, Class  _Nonnull moduleClass, BOOL * _Nonnull stop) {
        weakSelf.jsContext[@"native"][method] = ^(NSDictionary* params){
            if ([moduleClass isSubclassOfClass:[QLJsNativeBaseResponder class]]) {
                id obj = [[moduleClass alloc] initWithWebView:self.webView controller:self.controller];
                if ([obj isKindOfClass:[QLJsNativeBaseResponder class]]) {
                    NSString *methodSelector = [NSString stringWithFormat:@"%@:",method];
                    if ([obj respondsToSelector: NSSelectorFromString(methodSelector)]) {
                        WKWebView *webView = self.webView;
                        UIViewController *controller = self.controller;
                        if ([params isKindOfClass:[NSDictionary class]]) {
                            [obj performSelector: NSSelectorFromString(methodSelector) withObject:params];
                        }else{
                            NSLog(@"数据格式不对");
                            [weakSelf.webView stringByEvaluatingJavaScriptFromString:@"alert('数据格式不对')"];
                        }
                    }
                }else{
                    NSLog(@"没有找到方法");
                }
                
            }
        };
       
    }];
}
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    return YES;
}

@end
